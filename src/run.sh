#!/bin/bash

MINSHIPSIZE=$(jq '.minsize' <<< $UP42_TASK_PARAMETERS)
MAXSHIPSIZE=$(jq '.maxsize' <<< $UP42_TASK_PARAMETERS)
echo "minsize=$MINSHIPSIZE" >> /block/src/template/SD.properties
echo "maxsize=$MAXSHIPSIZE" >> /block/src/template/SD.properties

for f in /tmp/input/*/*.SAFE ; do
    export bn=$(basename $f)
    export outgj=$bn.geojson
    
    # Land-Sea-Mask
    gpt -e -p /block/src/template/SD.properties /block/src/template/SD1_mask.xml "$f" -t S1_subset_mask.dim
    # Calibration
    gpt -e -p /block/src/template/SD.properties /block/src/template/SD2_calibration.xml S1_subset_mask.dim -t S1_subset_mask_cal.dim
    # Object detection and geocoding
    gpt -e -p /block/src/template/SD.properties /block/src/template/SD3_Detection_early.xml S1_subset_mask_cal.dim -t S1_subset_mask_cal_objects.dim
    gpt -e -p /block/src/template/SD.properties /block/src/template/SD4_Detection_final.xml S1_subset_mask_cal_objects.dim -t final_out.dim
    
    tail -n +2 final_out.data/vector_data/ShipDetections.csv | awk -F "\"*\t\"*" '{print $5,$6}' > locationcols.csv
    
    ogr2ogr -f GeoJSON -oo X_POSSIBLE_NAMES=*lon* -oo Y_POSSIBLE_NAMES=*lat* "$outgj" locationcols.csv
    
    mkdir -p /tmp/output/
    mv "$outgj" /tmp/output/
    
done
