# Use debian stable as the base image.
FROM ubuntu:latest
#FROM osgeo/gdal

# Build argument defining the maximum heap size for gpt.
ARG MAX_HEAP_SIZE=14G


RUN apt-get update -y \
&& apt-get install -y wget jq gzip gdal-bin

RUN wget http://step.esa.int/downloads/7.0/installers/esa-snap_sentinel_unix_7_0.sh

RUN chmod 700 esa-snap_sentinel_unix_7_0.sh \
&& ./esa-snap_sentinel_unix_7_0.sh -q \
&& ln -s /usr/local/snap/bin/gpt /usr/bin/gpt \
&& sed -i -e "s/-Xmx1G/-Xmx${MAX_HEAP_SIZE}/g" /usr/local/snap/bin/gpt.vmoptions

ARG .
ARG manifest
LABEL "up42_manifest"=$manifest

COPY $BUILD_DIR/src/ /block/src/

WORKDIR /block

ENV LD_LIBRARY_PATH="."

CMD ["bash", "/block/src/run.sh"]
